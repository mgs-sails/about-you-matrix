<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MatrixTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function valid_matrices_can_be_multiplied()
    {
        $this->withoutExceptionHandling();
        $data = [
            'matrixOne' => [
                [0, 3, 5],
                [5, 5, 2]
            ],
            'matrixTwo' => [
                [3, 4],
                [3, -2],
                [4, -2]
            ],
        ];

        $response = $this->post('/api/matrices/multiply', $data, ['X-Requested-With' => 'XMLHttpRequest']);

        $response->assertStatus(200);
    }

    /** @test */
    public function first_matirx_cols_must_match_second_matrix_rows()
    {
        $data = [
            'matrixOne' => [
                [0, 3, 5, 1],
                [5, 5, 2, 1],
            ],
            'matrixTwo' => [
                [3, 4],
                [3, -2],
                [4, -2]
            ],
        ];

        $response = $this->withHeaders(
            [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
        )->json('POST', '/api/matrices/multiply', $data);

        $response->assertStatus(422);
    }

    /** @test */
    public function an_invalid_matrix_cannot_be_multiplied()
    {
        $data     = [
            'matrixOne' => [
                [0, 3, 5, 1],
                [5, 5, 2, 1, 6, 7, 8],
            ],
            'matrixTwo' => [
                [3, 4],
                [3, -2],
                [4, -2]
            ],
        ];
        $response = $this->withHeaders(
            [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
        )->json('POST', '/api/matrices/multiply', $data);

        $response->assertStatus(422);
    }
}

<?php


namespace App\Classes;

class Converter
{


    public static function convertToAlpha(int $num)
    {
        $num      = abs($num);
        $result   = '';
        $alphabet = range('A', 'Z');
        if (isset($alphabet[$num])) {
            $result .= $alphabet[$num];
        } else {
            $quotientIndex  = (int)($num / 26) - 1;
            $remainderIndex = $num % 26;
            if (isset($alphabet[$quotientIndex])) {
                $result .= $alphabet[$quotientIndex];
            } else {
                self::convertToAlpha($quotientIndex);
            }
            if (isset($alphabet[$remainderIndex])) {
                $result .= $alphabet[$remainderIndex];
            } else {
                self::convertToAlpha($remainderIndex);
            }
        }

        return $result;
    }

}

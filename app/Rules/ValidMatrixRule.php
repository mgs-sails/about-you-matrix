<?php
declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidMatrixRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $matrixTwo;

    public function __construct(array $matrixTwo)
    {
        $this->matrixTwo = $matrixTwo;
    }

    /**
     * Determine if all matrix rows, have the same number of columns.
     *
     * @param   string  $attribute
     * @param   mixed   $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $colsA = sizeof($value[0]);
        foreach ($value as $row) {
            if (sizeof($row) !== $colsA) {
                return false;
            }
        }
        $colsB = sizeof($this->matrixTwo[0]);
        foreach ($this->matrixTwo as $row) {
            if (sizeof($row) !== $colsB) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You entered an invalid matrix';
    }
}

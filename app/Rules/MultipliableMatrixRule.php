<?php
declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MultipliableMatrixRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $matrixTwo;

    public function __construct($matrixTwo)
    {
        $this->matrixTwo = $matrixTwo;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param   string  $attribute
     * @param   mixed   $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $colsA = sizeof($value[0]);
        $rowsB = sizeof($this->matrixTwo);

        return $colsA == $rowsB;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The number of cols in the first matrix must match the number of rows in the second matrix';
    }
}

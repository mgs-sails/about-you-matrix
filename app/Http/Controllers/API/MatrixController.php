<?php

namespace App\Http\Controllers\API;

use App\Classes\Converter;
use App\Http\Controllers\Controller;
use App\Http\Requests\MultiplicationRequest;

class MatrixController extends Controller
{
    public function multiply(MultiplicationRequest $request)
    {
        $matrixA = $request->matrixOne;
        $matrixB = $request->matrixTwo;
        $rowsA   = sizeof($matrixA);
        $colsA   = sizeof($matrixA[0]);
        $colsB   = sizeof($matrixB[0]);

        $resultInNumbers = [];
        $resultInAlpha   = [];

        for ($i = 0; $i < $rowsA; $i++) {
            for ($j = 0; $j < $colsB; $j++) {
                $sum = 0;
                for ($k = 0; $k < $colsA; $k++) {
                    $sum += $matrixA[$i][$k] * $matrixB[$k][$j];
                }
                $resultInNumbers[$i][$j] = $sum;
                $resultInAlpha[$i][$j]   = Converter::convertToAlpha($sum);
            }
        }

        return response()->json(
            [
                'resultInNumbers' => $resultInNumbers,
                'resultInAlpha' => $resultInAlpha
            ],
            200
        );
    }
}

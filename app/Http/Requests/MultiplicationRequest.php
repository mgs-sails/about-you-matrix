<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Rules\MultipliableMatrixRule;
use App\Rules\ValidMatrixRule;
use Illuminate\Foundation\Http\FormRequest;

class MultiplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'matrixOne.*.*' => ['required', 'numeric'],
            'matrixTwo.*.*' => ['required', 'numeric'],
            'matrixOne' => [
                new ValidMatrixRule($this->matrixTwo),
                new MultipliableMatrixRule($this->matrixTwo)
            ],
        ];
    }
}
